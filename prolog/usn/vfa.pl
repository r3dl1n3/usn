:- module(usn_vfa, []).

regiment(vfa).

squadron0(102).

squadron_name(vfa_102, "VFA-102 Diamondbacks").

squadron_type(vfa_102, "F-14B").

squadron_livery(vfa_102, "VF-102 Diamondbacks").

ordinal(1, '1st').
ordinal(2, '2nd').
ordinal(3, '3rd').
ordinal(4, '4th').

section0(Section0) :- between(1, 4, Section0).

element0(Element0) :- between(1, 4, Element0).

squadron(Squadron0, Squadron) :-
    squadron0(Squadron0),
    regiment(Regiment),
    atomic_list_concat([Regiment, Squadron0], '_', Squadron).

section(Squadron, Section0, Section) :-
    squadron(_, Squadron),
    section0(Section0),
    atomic_list_concat([Squadron, Section0], '_', Section).

element(Squadron, Section, Element0, Element) :-
    section(Squadron, _, Section),
    element0(Element0),
    atomic_concat(Section, Element0, Element).

section_name(Section, Name) :-
    section(Squadron, Section0, Section),
    squadron_name(Squadron, Name0),
    ordinal(Section0, Section_),
    format(string(Name), '~s ~s Section', [Name0, Section_]).

element_name(Element, Name) :-
    element(Squadron, Section, Element0, Element),
    section(Squadron, Section0, Section),
    squadron_name(Squadron, Name0),
    format(string(Name), '~s ~d-~d', [Name0, Section0, Element0]).

element_tail(Element, Tail) :-
    element(Squadron, Section, Element0, Element),
    section(Squadron, Section0, Section),
    format(string(Tail), 'V~d~d', [Section0, Element0]).

group(_World, Section, group{name:Name}) :-
    section_name(Section, Name).

unit(_World, Element, unit{name:Name}) :-
    element_name(Element, Name).
unit(_World, Element, unit{onboard_num:Tail}) :-
    once(element_tail(Element, Tail)).
unit(_World, Element, unit{type:Type}) :-
    element(Squadron, _, _, Element),
    squadron_type(Squadron, Type).
unit(_World, Element, unit{livery_id:Livery}) :-
    element(Squadron, _, _, Element),
    squadron_livery(Squadron, Livery).
unit(_World, Element, unit{skill:"Client"}) :-
    element(_, _, _, Element).
unit(_World, Element, unit{payload:Payload}) :-
    element(_, _, _, Element),
    payload(Element, Payload).
unit(World, Element, unit{callsign:Callsign}) :-
    callsign(World, Element, Callsign).

payload(_Element, payload{fuel:16200}).
payload(_Element, payload{gun:100}).
payload(_Element, payload{chaff:140}).
payload(_Element, payload{flare:60}).

callsign(_World, _Element, callsign{1:1}).
callsign(_World, Element, callsign{2:Section0}) :-
    element(_, Section, _Element0, Element),
    section(_, Section0, Section).
callsign(_World, Element, callsign{3:Element0}) :-
    element(_, _, Element0, Element).

:- listen(Squadron:country(usa),
          squadron(_, Squadron)).

:- listen(Squadron:element(Section, Element),
          (   squadron(_, Squadron),
              element(Squadron, Section, _, Element)
          )).

:- listen(Squadron:group(World, Section, Group),
          (   squadron(_, Squadron),
              group(World, Section, Group)
          )).

:- listen(Squadron:unit(World, Element, Unit),
          (   squadron(_, Squadron),
              unit(World, Element, Unit)
          )).
