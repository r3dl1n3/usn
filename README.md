# United States Navy (USN)

## Squadron prefixes

- VF, fighter squadrons
- VFA, strike-fighter squadrons

## References

- [VF-74, Fighter Squadron 74, Be-Devilers](https://en.wikipedia.org/wiki/VF-74)
- [Strike Fighter Squadron 102 Diamondbacks](https://en.wikipedia.org/wiki/VFA-102)
- [VFA-14 (U.S. Navy)](https://en.wikipedia.org/wiki/VFA-14_(U.S._Navy))
